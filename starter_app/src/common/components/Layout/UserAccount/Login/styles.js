import { Dimensions } from 'react-native';

const size = Dimensions.get('window');

export default {
	// Login container
	imageBackground: {
		width: '100%',
		height: '100%',
		alignItems: 'center',
		justifyContent: 'space-around',
	},
	container: {
		backgroundColor: '#fff',
		borderTopLeftRadius: 125,
		borderTopRightRadius: 125,
		width: '90%',
		marginTop: size.height*0.1,
	},
	logo: {
		marginTop: 50,
		alignSelf: 'center',
		width: 100,
		height: 100,
	},
	containerForm: {
		flex: 1,
		marginHorizontal: 32,
	},
	contentForm: {
		marginTop: 46,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	// Login form
	form: {
		flexDirection: 'column',
		width: '100%',
		alignItems: 'center',
		alignSelf: 'center',
		// marginBottom: 25,
	},
};