import React from 'react';
import { Text, View, Item, Input, Form } from 'native-base';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { emailFormat, required, alphaNumeric } from '../../../Base/validators';
import { ButtonGradientOutlinePrimary } from '../../../Base/buttons';
import { renderInputField } from '../../../Base/inputs';
import styles from './styles';

let LoginForm = props => {
    const { handleSubmit } = props;
    return (
        <Form style={styles.form}>
            <Field name="username" component={renderInputField}
                label="Usuario"
                validate={[required]}
            />
            <Field name="password" component={renderInputField}
                label="Contraseña"
                validate={[required]}
                secureTextEntry
            />
            <ButtonGradientOutlinePrimary label="INICIAR SESIÓN" onPress={() => handleSubmit(props)} />
        </Form>
    )
};

LoginForm.propTypes = {
	auth: PropTypes.object,
	loginForm: PropTypes.object,
	login: PropTypes.func
};

LoginForm = reduxForm({
	form: 'login',
	initialValues: {
		username: 'wilson',
		password: 'wilsongxx',
	},
})(LoginForm);

export default LoginForm;