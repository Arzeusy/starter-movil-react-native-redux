import React, { Component } from 'react';
import { Image, StatusBar, Alert, ImageBackground } from 'react-native';
import { Container, Content, View, Toast } from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import LoginForm from './LoginForm';
import styles from './styles';

const logo = require('../../../../../../assets/images/logo.png');
const fondo = require('../../../../../../assets/images/back-arboles.png');

export default class Login extends Component {

	UNSAFE_componentWillReceiveProps(nextProps, nextState) {
		if (this.props.auth.isFailed !== nextProps.auth.isFailed) {
			if (nextProps.auth.isFailed && !nextProps.auth.isAuthenticating) {
				let message = nextProps.auth.error;
				setTimeout(() => {
					Alert.alert('', message);
				}, 100);
			}
		}

		if (this.props.auth.token !== nextProps.auth.token) {
			if (nextProps.auth.token) {
				this.props.navigation.navigate('home');
			}
		}
	}

	login = ({ valid }) => {
		if (valid) {
			let { username, password } = this.props.loginForm.values;
			this.props.loginSubmit({ username, password });
		} else {
			Toast.show({
				text: 'Enter Valid Username & password!',
				duration: 2000,
				position: 'top',
				textStyle: { textAlign: 'center' }
			});
		}
	}

	render() {
		return (
			<ImageBackground source={fondo}
				style={styles.imageBackground}
			>
				<Container style={styles.container}>
					<StatusBar hidden={true} />
					<Spinner visible={this.props.auth.isAuthenticating} />
					<Content>
						<Image source={logo} style={styles.logo} />
						<View style={styles.containerForm}>
							<View style={styles.contentForm}>
								<LoginForm
									handleSubmit={this.login}
								/>
							</View>
						</View>
					</Content>
				</Container>
			</ImageBackground>
		);
	}
}