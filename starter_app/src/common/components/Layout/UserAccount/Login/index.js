import { connect } from 'react-redux';
import { loginSubmit } from '../../../../../redux/modules/auth';
import Login from './Login';

const mstp = state => ({
	auth: state.auth,
	loginForm: state.form.login
});
const mdtp = {
    loginSubmit,
};

export default connect(mstp, mdtp)(Login);