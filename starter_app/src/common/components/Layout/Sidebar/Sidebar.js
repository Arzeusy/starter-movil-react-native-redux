import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Content, Text, List, ListItem, Container, View, Icon } from 'native-base';
import _ from 'lodash';

const avatarImage = require('../../../../../assets/avatar.png');

export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.data = [
            {
                name: 'Home',
                route: 'home',
                icon: 'home'
            },
            {
                name: 'Modal',
                route: 'modal',
                icon: 'albums'
            },
            {
                name: 'Logout',
                route: 'logout',
                icon: 'log-out'
            }
        ];
    }

    navigator(data) {
        if (data.route === 'logout') {
            this.props.logoutSubmit();
            this.props.navigation.navigate('login');
        } else {
            this.props.navigation.navigate(data.route);
        }
    }

    render() {
		let logoutMenu = _.find(this.data, { route: 'logout' });
        return (
            <Container style={styles.container}>
                <View style={styles.drawerCover}>
                    <Image source={avatarImage} style={styles.avatar} />
                    <Text style={styles.organizerName}>Demo Account</Text>
                </View>
                <Content bounces={false}>
                    <List
                        dataArray={this.data}
                        renderRow={data => {
                            if (data.route === 'logout') {
                                return null;
                            } else {
                                return <ListItem style={styles.menuItem} onPress={() => this.navigator(data)}>
                                    <Icon
                                        active
                                        name={data.icon}
                                        style={{ color: '#777', fontSize: 26, width: 30 }}
                                    />
                                    <Text style={styles.menuText}>{data.name}</Text>
                                </ListItem>;
                            }
                        }}
                    />
                </Content>
                <TouchableOpacity style={styles.footer} onPress={() => this.navigator(logoutMenu)}>
                    <Icon
                        active
                        name={logoutMenu.icon}
                        style={{ color: '#777', fontSize: 26, width: 30 }}
                    />
                    <Text style={styles.menuText}>{logoutMenu.name}</Text>
                </TouchableOpacity>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F2F2F2",
    },
    drawerCover: {
        backgroundColor: "#fff",
        height: 104,
        flexDirection: "row",
        alignItems: "center",
    },
    avatar: {
        width: 56,
        height: 56,
        borderRadius: 28,
        borderWidth: 1,
        borderColor: "#64C8D0",
        marginLeft: 16
    },
    organizerName: {
        marginLeft: 16,
        fontSize: 17,
        fontWeight: "bold",
        marginRight: 16,
        flex: 1
    },
    menuItem: {
        height: 70
    },
    menuText: {
        marginLeft: 16,
        fontSize: 17
    },
    footer: {
        marginLeft: 16,
        flexDirection: "row",
        height: 70
    }
});