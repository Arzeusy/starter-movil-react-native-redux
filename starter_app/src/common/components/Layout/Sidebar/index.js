import { connect } from 'react-redux';
import { actions } from '../../../../redux/modules/auth';
import Sidebar from './Sidebar';

const mdtp = {
    ...actions
};

export default connect(null, mdtp)(Sidebar);