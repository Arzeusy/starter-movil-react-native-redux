import React, { Component } from 'react';
import { StyleSheet, Image, StatusBar, ActivityIndicator } from 'react-native';
import { Container, Text, View } from 'native-base';

const logo = require('../../../../../assets/logoApp.png');

export default class Loading extends Component {
    constructor(props) {
        super(props);
        // Se verifica si está autenticado con una cuenta de usuario válida.
        // Para ello, debe tener un token de autenticación y que sea válido
        props.getMe();
    }
    UNSAFE_componentWillReceiveProps(nextProps, nextState) {
        if (this.props.auth.loader !== nextProps.auth.loader && !nextProps.auth.loader) {
            this.props.navigation.navigate(this.props.auth.token ? 'home' : 'login');
        }
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar hidden={true} />
                <Image source={logo} style={styles.logo} />
                <Text style={styles.textLogo}>For CME Organizer</Text>
                <View style={styles.containerLoading}>
                    <ActivityIndicator size="large" color="#5ABEEC" />
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    logo: {
        marginTop: 104,
        alignSelf: 'center',
        width: 238,
        height: 49
    },
    textLogo: {
        marginTop: 18,
        alignSelf: 'center',
        fontSize: 20
    },
    containerLoading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});