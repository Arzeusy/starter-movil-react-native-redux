import { connect } from 'react-redux';
import { getMe } from '../../../../redux/modules/auth';
import Loading from './Loading';

const mstp = state => ({
	auth: state.auth,
});
const mdtp = {
	getMe,
}

export default connect(mstp, mdtp)(Loading);