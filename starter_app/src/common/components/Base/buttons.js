import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text, View } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import styles from './style-buttons';

const colorGradientPrimary = ['#55c4d4', '#8ec640'];
const colorGradientDark = ['#ebebeb', '#6e6e6e'];

export const ButtonGradientPrimary = props => {
    const { label, onPress } = props;
    return (
        <TouchableOpacity onPress={onPress} style={styles.baseButton}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                colors={colorGradientPrimary}
                style={styles.outsideRadius}
            >
                <Text style={styles.textButton}>{label}</Text>
            </LinearGradient>
        </TouchableOpacity>
    )
};

export const ButtonGradientOutlinePrimary = props => {
    const { label, onPress } = props;
    return (
        <TouchableOpacity onPress={onPress} style={styles.baseButton}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                colors={colorGradientPrimary}
                style={styles.outsideRadius}
            >
                <View style={styles.insideRadius}>
                    <Text style={styles.textButtonPrimary}>{label}</Text>
                </View>
            </LinearGradient>
        </TouchableOpacity>
    )
};

export const ButtonGradientDark = props => {
    const { label, onPress } = props;
    return (
        <TouchableOpacity onPress={onPress} style={styles.baseButton}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                colors={colorGradientDark}
                style={styles.outsideRadius}
            >
                <Text style={styles.textButton}>{label}</Text>
            </LinearGradient>
        </TouchableOpacity>
    )
};

export const ButtonGradientOutlineDark = props => {
    const { label, onPress } = props;
    return (
        <TouchableOpacity onPress={onPress} style={styles.baseButton}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                colors={colorGradientDark}
                style={styles.outsideRadius}
            >
                <View style={styles.insideRadius}>
                    <Text style={styles.textButtonDark}>{label}</Text>
                </View>
            </LinearGradient>
        </TouchableOpacity>
    )
};