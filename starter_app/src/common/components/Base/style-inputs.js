export default {
	itemInputForm: {
		marginVertical: 15,
		marginLeft: 0,
		borderBottomWidth: 0,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'flex-start',
		width: '100%',
    },
	inputText: {
		fontSize: 17,
		color: '#035192',
		backgroundColor: 'white',
		borderWidth: 1,
		borderColor: '#55c4d4',
		borderRadius: 25,
		paddingLeft: 15,
		width: '100%',
	},
	inputTextError: {
		borderColor: 'red',
	},
};