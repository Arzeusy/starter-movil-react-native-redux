import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text, View, Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import styles from './style-cards';

const colorGradientPrimary = ['#8ec640', '#55c4d4', '#55c4d4'];

export const CardEmpty = props => {
    const { titulo, subtitulo } = props;
    return (
        <Container style={styles.containerCard}>
            <Container>
                <LinearGradient
                    start={{ x: 1, y: 0 }} end={{ x: 0, y: 1 }}
                    colors={colorGradientPrimary}
                    style={styles.outsideRadius}
                    locations={[0, 0.7, 1]}
                    useAngle={true} angle={-75} angleCenter={{ x: 0.5, y: 0.5 }}
                >
                    <View style={styles.insideRadius}>
                        {props.children}
                    </View>
                </LinearGradient>
            </Container>
        </Container>
    )
};