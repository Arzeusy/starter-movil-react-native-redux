export default {
    containerCard: {
        padding: 15,
        width: '100%',
        height: '100%',
    },
    outsideRadius: {
        padding: 2,
        justifyContent: 'center',
        alignItems: 'center',
		borderRadius: 25,
		width: '100%',
		height: '100%',
    },
	insideRadius: {
        backgroundColor: 'white',
        borderRadius: 25,
        width: '100%',
        height: '100%',
    },
};