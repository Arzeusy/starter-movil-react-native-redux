import React from 'react';
import { Text, View, Item, Input, Container } from 'native-base';
import styles from './style-inputs';

export const renderInputField = props => {
    const {
        input, label, type, placeholder, meta: { touched, error, warning },
        keyboardType, secureTextEntry,
    } = props;
    const hasError = error && touched;
    return (
        <Item error={hasError} style={styles.itemInputForm}>
            <View style={{ flexDirection: 'row', marginBottom: 5, marginHorizontal: 15, }}>
                <Text style={{ fontSize: 18, marginLeft: 10, color: '#035192', }}>{label}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                <Container style={{height: '100%', flexDirection: 'column'}}>
                    <Input ref={c => (this.textInput = c)}
                        placeholder={placeholder}
                        style={[styles.inputText, hasError ? styles.inputTextError : {}]}
                        keyboardType={keyboardType ? keyboardType : 'default'}
                        secureTextEntry={secureTextEntry}
                        {...input}
                    />
                    { hasError && <Text style={{ fontSize: 10, marginLeft: 10, color: 'red' }}>{error}</Text> }
                </Container>
            </View>
        </Item>
    );
};