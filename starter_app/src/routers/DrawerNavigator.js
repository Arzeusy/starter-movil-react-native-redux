import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
// Components
import { SideBar } from '../common/components/Layout';
import { Home, Modal } from '../common/components/Demo';

const RootDrawer = createDrawerNavigator(
	{
		home:   { screen: Home },
		modal:  { screen: Modal },
	},
	{
		initialRouteName: 'home',
		contentComponent: props => <SideBar {...props} />
	}
);
const DrawerNavigator = createAppContainer(RootDrawer);

export default DrawerNavigator;