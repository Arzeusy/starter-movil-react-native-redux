import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
// Components
import Drawer from './DrawerNavigator';
import { Login } from '../common/components/Layout/UserAccount';

const ProtectedStack = createStackNavigator(
	{
		drawer: { screen: Drawer },
	},
	{
		initialRouteName: 'drawer',
		headerMode: 'none'
	}
);
const AuthStack = createStackNavigator(
	{
		login: { screen: Login },
	},
	{
		initialRouteName: 'login',
		headerMode: 'none'
	}
);

export const ProtectedNavigator = createAppContainer(ProtectedStack);
export const AuthNavigator = createAppContainer(AuthStack);