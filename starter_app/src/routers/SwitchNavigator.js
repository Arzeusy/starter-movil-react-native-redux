import { createAppContainer, createSwitchNavigator } from 'react-navigation';
// Components
// import App from './AppNavigator';
import { Loading } from '../common/components/Layout';
import { AuthNavigator, ProtectedNavigator } from './AppNavigator';

const Routes = createSwitchNavigator(
	{
		initialLoading: Loading,
		auth: AuthNavigator,
		protected: ProtectedNavigator,
	},
	{
		initialRouteName: 'initialLoading'
	}
);
export default SwitchNavigator = createAppContainer(Routes);